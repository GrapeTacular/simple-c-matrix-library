#ifndef TEST_FUNCS_HPP
#define TEST_FUNCS_HPP

#include <iostream>
#include <random>

#include "../src/Matrix.hpp"

template<typename T, SimpleMatrix::size_type ROWS, SimpleMatrix::size_type COLS>
void print(const SimpleMatrix::Matrix<T, ROWS, COLS, const T*>& M) {
    for (SimpleMatrix::size_type r = 0; r < ROWS; ++r) {
        for (SimpleMatrix::size_type c = 0; c < COLS; ++c)
            std::cout << M(r, c) << ' ';

        std::cout << '\n';
    }

    std::cout << std::endl;
}

template<typename T, SimpleMatrix::size_type ROWS, SimpleMatrix::size_type COLS>
void makeRandomIntMatrix(SimpleMatrix::Matrix<T, ROWS, COLS, const T*>& M, int bottom, int top) {
    std::random_device rand_dev;
    std::mt19937 generator(rand_dev());
    std::uniform_int_distribution<int> distribution(bottom, top);

    for (SimpleMatrix::size_type r = 0; r < ROWS; ++r)
        for (SimpleMatrix::size_type c = 0; c < COLS; ++c)
            M(r, c) = distribution(generator);
}

#endif