#include <iostream>

#include "../src/Matrix.hpp"
#include "../src/BasicMatrixAlgebraFuncs.hpp"

#include "TestFuncs.hpp"


int main() {
    using namespace SimpleMatrix;
    
    
    std::cout << "operators with the same dimensions\n";
    std::cout << std::endl;

    Matrix<double, 2, 2> A = {
        1.0, 2.0,
        3.0, 4.0
    };

    Matrix<double, 2, 2> B = {
        1.0, 2.0,
        3.0, 4.0
    };

    Matrix<double, 2, 2> C = 2.0 * A + B - A;

    print(A);
    print(B);
    print(C);

    Matrix<double, 2, 2> D = A * B;

    print(D);

    Matrix<double, 2, 2> E = A * B + A;

    print(E);


    std::cout << "\n\noperators with different dimensions\n";
    std::cout << std::endl;

    Matrix<double, 1, 2> A1 = {
        1.0, 2.0
    };

    Matrix<double, 2, 1> B1 = {
        1.0,
        2.0
    };

    Matrix<double, 1, 1> C1 = {
        1.0
    };

    Matrix<double, 1, 1> D1 = A1 * B1;
    Matrix<double, 1, 1> D2 = A1 * B1 + 2.0 * C1;
    Matrix<double, 1, 1> D3 = A1 * B1 - 3.0 * C1;

    print(A1);
    print(B1);
    print(D1);
    print(D2);
    print(D3);


    std::cout << "\n\nBasic Matrix Functions\n";
    std::cout << std::endl;

    Matrix<double, 2, 2> M = {
        1, 2,
        3, 4
    };

    print(M);
    std::cout << getDeterminant(M, 0.0001) << '\n' << std::endl;
    putZerosUnderPivots(M, 0.0001);
    print(M);
    std::cout << std::endl;
    putZerosAbovePivots(M, 0.0001);
    print(M);
    std::cout << std::endl;
    divideRowsByPivots(M, 0.00001);
    print(M);
    std::cout << std::endl;

    Matrix<double, 3, 6> N = {
        0, 3, -6, 6, 4, -5,
        3, -7, 8, -5, 8, 9,
        3, -9, 12, -9, 6, 15
    };

    print(N);
    std::cout << std::endl;
    putZerosUnderPivots(N, 0.00001);
    print(N);
    std::cout << std::endl;
    divideRowsByPivots(N, 0.00001);
    print(N);
    std::cout << std::endl;
    putZerosAbovePivots(N, 0.00001);
    print(N);
    std::cout << std::endl;


    std::cout << "\n\nRREF\n";
    std::cout << std::endl;

    Matrix<double, 4, 5> X;
    makeRandomIntMatrix(X, 0, 4);

    print(X);
    std::cout << std::endl;

    Matrix<double, 4, 5> Y = getReducedRowEchelonForm(X, 0.000001);
    print(Y);

    return 0;
}