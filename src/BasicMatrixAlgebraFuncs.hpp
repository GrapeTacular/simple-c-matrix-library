#ifndef BASIC_MATRIX_ALGEBRA_FUNCS_HPP
#define BASIC_MATRIX_ALGEBRA_FUNCS_HPP

namespace SimpleMatrix {

template<typename T>
bool isZero(const std::complex<T>& value, T positiveMin) {
    T real = value.real();
    T imag = value.imag();

    return real > -positiveMin && real < positiveMin && imag > -positiveMin && imag < positiveMin;
}

template<typename T>
bool isZero(const T& value, T positiveMin) {
    return value > -positiveMin && value < positiveMin;
}

template<typename T>
void makeZero(std::complex<T>& value) {
    value.real(0);
    value.imag(0);
}

template<typename T>
void makeZero(T& value) {
    value = 0;
}

template<typename T>
void makeOne(std::complex<T>& value) {
    value.real(1);
    value.imag(0);
}

template<typename T>
void makeOne(T& value) {
    value = 1;
}

template<typename T, size_type ROWS, size_type COLS>
void exchangeRows(Matrix<T, ROWS, COLS, const T*>& M, size_type firstRow, size_type secondRow) {
    if (firstRow < 0 || secondRow < 0)
        throw std::invalid_argument("row indices must be greater than or equal to 0");

    if (firstRow >= ROWS || secondRow >= ROWS)
        throw std::invalid_argument("row indices must be less than matrix row total");

    T intermediate;

    for (size_type c = 0; c < COLS; ++c) {
        intermediate = M(firstRow, c);
        M(firstRow, c) = M(secondRow, c);
        M(secondRow, c) = intermediate;
    }
}

template<typename T, size_type ROWS, size_type COLS>
void roundToZeroAtMin(Matrix<T, ROWS, COLS, const T*>& M, T positiveMin) {
    for (size_type r = 0; r < ROWS; ++r) {
        for (size_type c = 0; c < COLS; ++c) {
            if (isZero(M(r, c), positiveMin))
                makeZero(M(r, c));
        }
    }
}

template<typename T, size_type ROWS, size_type COLS>
int partialSort(Matrix<T, ROWS, COLS, const T*>& M, size_type row, size_type col, T positiveMin) {
    for (size_type r = row + 1; r < ROWS; ++r) {
        if (isZero(M(r, col), positiveMin))
            continue;

        exchangeRows(M, row, r);
        return -1;
    }

    return 1;
}

template<typename T, size_type ROWS, size_type COLS>
int putZerosUnderPivots(Matrix<T, ROWS, COLS, const T*>& M, T positiveMin) {
    int determinantMultiplier = 1;

    size_type r = 0;
    size_type c = 0;

    while (r < ROWS && c < COLS) {
        if (isZero(M(r, c), positiveMin))
            determinantMultiplier *= partialSort(M, r, c, positiveMin);

        if (isZero(M(r, c), positiveMin)) {
            ++c;
        } else {
            T pivot = M(r, c);

            for (size_type ri = r + 1; ri < ROWS; ++ri) {
                T multiplier = M(ri, c) / pivot;

                for (size_type ci = c + 1; ci < COLS; ++ci)
                    M(ri, ci) -= M(r, ci) * multiplier;

                makeZero(M(ri, c));
            }

            ++r;
            ++c;
        }
    }

    return determinantMultiplier;
}

template<typename T, size_type DIMENSION>
T getDeterminant(const Matrix<T, DIMENSION, DIMENSION, const T*>& M, T positiveMin) {
    Matrix<T, DIMENSION, DIMENSION, const T*> R = M;

    T determinant = putZerosUnderPivots(R, positiveMin);

    for (size_type i = 0; i < DIMENSION; ++i)
        determinant *= R(i, i);

    return determinant;
}

template<typename T, size_type ROWS, size_type COLS>
void divideRowsByPivots(Matrix<T, ROWS, COLS, const T*>& M, T positiveMin) {
    for (size_type r = 0; r < ROWS; ++r) {
        for (size_type c = 0; c < COLS; ++c) {
            if (isZero(M(r, c), positiveMin))
                continue;

            T pivot = M(r, c);
            makeOne(M(r, c));

            for (size_type ci = c + 1; ci < COLS; ++ci) {
                if (isZero(M(r, ci), positiveMin))
                    continue;

                M(r, ci) /= pivot;
            }

            break;
        }
    }
}

template<typename T, size_type ROWS, size_type COLS>
void putZerosAbovePivots(Matrix<T, ROWS, COLS, const T*>& M, T positiveMin) {
    for (int r = ROWS - 1; r >= 0; --r) {
        for (size_type c = 0; c < COLS; ++c) {
            if (isZero(M(r, c), positiveMin))
                continue;

            T pivot = M(r, c);

            for (int ri = r - 1; ri >= 0; --ri) {
                T multiplier = M(ri, c) / pivot;

                for (size_type ci = c + 1; ci < COLS; ++ci)
                    M(ri, ci) -= M(r, ci) * multiplier;

                makeZero(M(ri, c));
            }

            break;
        }
    }
}

template<typename T, size_type ROWS, size_type COLS>
Matrix<T, ROWS, COLS, const T*> getReducedRowEchelonForm(Matrix<T, ROWS, COLS, const T*>& M, T positiveMin) {
    Matrix<T, ROWS, COLS, const T*> R = M;

    putZerosUnderPivots(R, positiveMin);
    putZerosAbovePivots(R, positiveMin);
    divideRowsByPivots(R, positiveMin);
    roundToZeroAtMin(R, positiveMin);

    return R;
}

}

#endif