#ifndef MATRIX_HPP
#define MATRIX_HPP

#include <initializer_list>
#include <complex>

namespace SimpleMatrix {

using size_type = std::size_t;

template<typename T, size_type ROWS, size_type COLS, typename R = const T*>
class Matrix {
private:
    R representation;

public:
    Matrix(const R& rep) : representation(rep) {};
    Matrix(const Matrix&) = default;
    ~Matrix() = default;

    auto operator [] (size_type k) const { return this->representation[k]; }

    R getRepresentation() const { return this->representation; }
};

template<typename T, size_type ROWS, size_type COLS>
class Matrix<T, ROWS, COLS, const T*> {
private:
    T elements[ROWS * COLS];

public:
    Matrix() = default;
    Matrix(const Matrix&) = default;
    Matrix(Matrix&&) = default;

    template<typename R>
    Matrix(const Matrix<T, ROWS, COLS, R>& M) {
        size_type size = ROWS * COLS;

        for (size_type k = 0; k < size; ++k)
            this->elements[k] = M[k];
    }

    Matrix(std::initializer_list<T> list) {
        size_type k = 0;

        for (auto& e : list)
            this->elements[k++] = e;
    }

    ~Matrix() = default;

    Matrix& operator = (const Matrix&) = default;
    Matrix& operator = (Matrix&&) = default;

    template<typename R>
    Matrix& operator = (const Matrix<T, ROWS, COLS, R>& M) {
        size_type size = ROWS * COLS;

        for (size_type k = 0; k < size; ++k)
            this->elements[k] = M[k];

        return *this;
    }

    T& operator [] (size_type k) { return this->elements[k]; }
    const T& operator [] (size_type k) const { return this->elements[k]; }

    T& operator () (size_type r, size_type c) { return this->elements[r * COLS + c]; }
    const T& operator () (size_type r, size_type c) const { return this->elements[r * COLS + c]; }

    const T* getRepresentation() const { return this->elements; }
};

template<size_type ROWS, size_type COLS, typename R1, typename R2>
class Sum {
private:
    R1 representation1;
    R2 representation2;

public:
    Sum(const R1& rep1, const R2& rep2) : representation1(rep1), representation2(rep2) {}
    Sum(const Sum&) = default;
    ~Sum() = default;

    Sum& operator = (const Sum&) = delete;

    auto operator [] (size_type k) const {
        return this->representation1[k] + this->representation2[k];
    }
};

template<typename T, size_type ROWS, size_type COLS, typename R1, typename R2>
auto operator + (const Matrix<T, ROWS, COLS, R1>& M1, const Matrix<T, ROWS, COLS, R2>& M2) {
    using R = Sum<ROWS, COLS, R1, R2>;
    return Matrix<T, ROWS, COLS, R>(R(M1.getRepresentation(), M2.getRepresentation()));
}

template<size_type ROWS, size_type COLS, typename R1, typename R2>
class Difference {
private:
    R1 representation1;
    R2 representation2;

public:
    Difference(const R1& rep1, const R2& rep2) : representation1(rep1), representation2(rep2) {}
    Difference(const Difference&) = default;
    ~Difference() = default;

    Difference& operator = (const Difference&) = delete;

    auto operator [] (size_type k) const {
        return this->representation1[k] - this->representation2[k];
    }
};

template<typename T, size_type ROWS, size_type COLS, typename R1, typename R2>
auto operator - (const Matrix<T, ROWS, COLS, R1>& M1, const Matrix<T, ROWS, COLS, R2>& M2) {
    using R = Difference<ROWS, COLS, R1, R2>;
    return Matrix<T, ROWS, COLS, R>(R(M1.getRepresentation(), M2.getRepresentation()));
}

template<size_type ROWS_R1, size_type INTERMEDIATE_K, size_type COLS_R2, typename R1, typename R2>
class Product {
private:
    R1 represenation1;
    R2 represenation2;

public:
    Product(const R1& rep1, const R2& rep2) : represenation1(rep1), represenation2(rep2) {}
    Product(const Product&) = default;
    ~Product() = default;

    Product& operator = (const Product&) = delete;

    auto operator [] (size_type k) const {
        size_type stride_r1 = (k / ROWS_R1) * ROWS_R1;
        size_type col_r2 = k % COLS_R2;

        auto value = this->represenation1[stride_r1] * this->represenation2[col_r2];

        for (size_type ik = 1; ik < INTERMEDIATE_K; ++ik)
            value += this->represenation1[stride_r1 + ik] * this->represenation2[ik * COLS_R2 + col_r2];

        return value;
    }
};

template<typename T, size_type ROWS_R1, size_type K, size_type COLS_R2, typename R1, typename R2>
auto operator * (const Matrix<T, ROWS_R1, K, R1>& M1, const Matrix<T, K, COLS_R2, R2>& M2) {
    using R = Product<ROWS_R1, K, COLS_R2, R1, R2>;
    return Matrix<T, ROWS_R1, COLS_R2, R>(R(M1.getRepresentation(), M2.getRepresentation()));
}

template<typename R1, typename R2>
class ScalarProduct {
private:
    R1 represenation1;
    R2 represenation2;

public:
    ScalarProduct(const R1& rep1, const R2& rep2) : represenation1(rep1), represenation2(rep2) {}
    ScalarProduct(const ScalarProduct&) = default;
    ~ScalarProduct() = default;

    ScalarProduct& operator = (const ScalarProduct&) = delete;

    auto operator [] (size_type k) const {
        return this->represenation1[k] * this->represenation2[k];
    }
};

template<typename T>
class Scalar {
private:
    T const& s;

public:
    Scalar(const T& s) : s(s) {}
    Scalar(const Scalar&) = default;
    ~Scalar() = default;

    Scalar& operator = (const Scalar&) = delete;

    const T& operator [] (size_type k) const { return s; }
};

template<typename T, size_type ROWS, size_type COLS, typename R2>
auto operator * (const T& s, const Matrix<T, ROWS, COLS, R2>& M2) {
    using R = ScalarProduct<Scalar<T>, R2>;
    return Matrix<T, ROWS, COLS, R>(R(s, M2.getRepresentation()));
}

}

#endif